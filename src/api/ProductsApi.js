/**
 * techubank_api-client
 * This API allows to interact with the amazing TechUBank
 *
 * OpenAPI spec version: 2.0.0
 * Contact: techubank@bbva.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ArrayOfUserProducts', 'model/ErrorMessage'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/ArrayOfUserProducts'), require('../model/ErrorMessage'));
  } else {
    // Browser globals (root is window)
    if (!root.TechubankApiClient) {
      root.TechubankApiClient = {};
    }
    root.TechubankApiClient.ProductsApi = factory(root.TechubankApiClient.ApiClient, root.TechubankApiClient.ArrayOfUserProducts, root.TechubankApiClient.ErrorMessage);
  }
}(this, function(ApiClient, ArrayOfUserProducts, ErrorMessage) {
  'use strict';

  /**
   * Products service.
   * @module api/ProductsApi
   * @version 2.0.0
   */

  /**
   * Constructs a new ProductsApi. 
   * @alias module:api/ProductsApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the getUserProducts operation.
     * @callback module:api/ProductsApi~getUserProductsCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ArrayOfUserProducts} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get user products
     * Get user products
     * @param {String} username 
     * @param {module:api/ProductsApi~getUserProductsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/ArrayOfUserProducts}
     */
    this.getUserProducts = function(username, callback) {
      var postBody = null;

      // verify the required parameter 'username' is set
      if (username === undefined || username === null) {
        throw new Error("Missing the required parameter 'username' when calling getUserProducts");
      }


      var pathParams = {
        'username': username
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['APIKeyHeader'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = ArrayOfUserProducts;

      return this.apiClient.callApi(
        '/products/{username}', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
