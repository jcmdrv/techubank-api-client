/**
 * techubank_api-client
 * This API allows to interact with the amazing TechUBank
 *
 * OpenAPI spec version: 2.0.0
 * Contact: techubank@bbva.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.TechubankApiClient) {
      root.TechubankApiClient = {};
    }
    root.TechubankApiClient.UserBasicInfo = factory(root.TechubankApiClient.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The UserBasicInfo model module.
   * @module model/UserBasicInfo
   * @version 2.0.0
   */

  /**
   * Constructs a new <code>UserBasicInfo</code>.
   * @alias module:model/UserBasicInfo
   * @class
   */
  var exports = function() {
    var _this = this;




  };

  /**
   * Constructs a <code>UserBasicInfo</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/UserBasicInfo} obj Optional instance to populate.
   * @return {module:model/UserBasicInfo} The populated <code>UserBasicInfo</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('username')) {
        obj['username'] = ApiClient.convertToType(data['username'], 'String');
      }
      if (data.hasOwnProperty('email')) {
        obj['email'] = ApiClient.convertToType(data['email'], 'String');
      }
      if (data.hasOwnProperty('userStatus')) {
        obj['userStatus'] = ApiClient.convertToType(data['userStatus'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {String} username
   */
  exports.prototype['username'] = undefined;
  /**
   * @member {String} email
   */
  exports.prototype['email'] = undefined;
  /**
   * @member {module:model/UserBasicInfo.UserStatusEnum} userStatus
   */
  exports.prototype['userStatus'] = undefined;


  /**
   * Allowed values for the <code>userStatus</code> property.
   * @enum {String}
   * @readonly
   */
  exports.UserStatusEnum = {
    /**
     * value: "ACTIVE"
     * @const
     */
    "ACTIVE": "ACTIVE",
    /**
     * value: "BLOCKED"
     * @const
     */
    "BLOCKED": "BLOCKED"  };


  return exports;
}));


