/**
 * techubank_api-client
 * This API allows to interact with the amazing TechUBank
 *
 * OpenAPI spec version: 2.0.0
 * Contact: techubank@bbva.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.TechubankApiClient) {
      root.TechubankApiClient = {};
    }
    root.TechubankApiClient.UserResetPassword = factory(root.TechubankApiClient.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The UserResetPassword model module.
   * @module model/UserResetPassword
   * @version 2.0.0
   */

  /**
   * Constructs a new <code>UserResetPassword</code>.
   * @alias module:model/UserResetPassword
   * @class
   */
  var exports = function() {
    var _this = this;





  };

  /**
   * Constructs a <code>UserResetPassword</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/UserResetPassword} obj Optional instance to populate.
   * @return {module:model/UserResetPassword} The populated <code>UserResetPassword</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('username')) {
        obj['username'] = ApiClient.convertToType(data['username'], 'String');
      }
      if (data.hasOwnProperty('oldpassword')) {
        obj['oldpassword'] = ApiClient.convertToType(data['oldpassword'], 'String');
      }
      if (data.hasOwnProperty('newpassword')) {
        obj['newpassword'] = ApiClient.convertToType(data['newpassword'], 'String');
      }
      if (data.hasOwnProperty('renewpassword')) {
        obj['renewpassword'] = ApiClient.convertToType(data['renewpassword'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {String} username
   */
  exports.prototype['username'] = undefined;
  /**
   * @member {String} oldpassword
   */
  exports.prototype['oldpassword'] = undefined;
  /**
   * @member {String} newpassword
   */
  exports.prototype['newpassword'] = undefined;
  /**
   * @member {String} renewpassword
   */
  exports.prototype['renewpassword'] = undefined;



  return exports;
}));


