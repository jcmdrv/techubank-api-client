/**
 * techubank_api-client
 * This API allows to interact with the amazing TechUBank
 *
 * OpenAPI spec version: 2.0.0
 * Contact: techubank@bbva.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.TechubankApiClient) {
      root.TechubankApiClient = {};
    }
    root.TechubankApiClient.Movement = factory(root.TechubankApiClient.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The Movement model module.
   * @module model/Movement
   * @version 2.0.0
   */

  /**
   * Constructs a new <code>Movement</code>.
   * @alias module:model/Movement
   * @class
   */
  var exports = function() {
    var _this = this;



  };

  /**
   * Constructs a <code>Movement</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Movement} obj Optional instance to populate.
   * @return {module:model/Movement} The populated <code>Movement</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('origin')) {
        obj['origin'] = ApiClient.convertToType(data['origin'], 'String');
      }
      if (data.hasOwnProperty('destination')) {
        obj['destination'] = ApiClient.convertToType(data['destination'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {String} origin
   */
  exports.prototype['origin'] = undefined;
  /**
   * @member {String} destination
   */
  exports.prototype['destination'] = undefined;



  return exports;
}));


