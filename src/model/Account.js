/**
 * techubank_api-client
 * This API allows to interact with the amazing TechUBank
 *
 * OpenAPI spec version: 2.0.0
 * Contact: techubank@bbva.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/UserBasicInfo'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./UserBasicInfo'));
  } else {
    // Browser globals (root is window)
    if (!root.TechubankApiClient) {
      root.TechubankApiClient = {};
    }
    root.TechubankApiClient.Account = factory(root.TechubankApiClient.ApiClient, root.TechubankApiClient.UserBasicInfo);
  }
}(this, function(ApiClient, UserBasicInfo) {
  'use strict';




  /**
   * The Account model module.
   * @module model/Account
   * @version 2.0.0
   */

  /**
   * Constructs a new <code>Account</code>.
   * @alias module:model/Account
   * @class
   */
  var exports = function() {
    var _this = this;










  };

  /**
   * Constructs a <code>Account</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Account} obj Optional instance to populate.
   * @return {module:model/Account} The populated <code>Account</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('product_type')) {
        obj['product_type'] = ApiClient.convertToType(data['product_type'], 'String');
      }
      if (data.hasOwnProperty('accountNumber')) {
        obj['accountNumber'] = ApiClient.convertToType(data['accountNumber'], 'String');
      }
      if (data.hasOwnProperty('title')) {
        obj['title'] = ApiClient.convertToType(data['title'], 'String');
      }
      if (data.hasOwnProperty('holder')) {
        obj['holder'] = UserBasicInfo.constructFromObject(data['holder']);
      }
      if (data.hasOwnProperty('co_holder')) {
        obj['co_holder'] = UserBasicInfo.constructFromObject(data['co_holder']);
      }
      if (data.hasOwnProperty('account_type')) {
        obj['account_type'] = ApiClient.convertToType(data['account_type'], 'String');
      }
      if (data.hasOwnProperty('creation_date')) {
        obj['creation_date'] = ApiClient.convertToType(data['creation_date'], 'String');
      }
      if (data.hasOwnProperty('status')) {
        obj['status'] = ApiClient.convertToType(data['status'], 'String');
      }
      if (data.hasOwnProperty('description')) {
        obj['description'] = ApiClient.convertToType(data['description'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {String} product_type
   */
  exports.prototype['product_type'] = undefined;
  /**
   * @member {String} accountNumber
   */
  exports.prototype['accountNumber'] = undefined;
  /**
   * @member {String} title
   */
  exports.prototype['title'] = undefined;
  /**
   * @member {module:model/UserBasicInfo} holder
   */
  exports.prototype['holder'] = undefined;
  /**
   * @member {module:model/UserBasicInfo} co_holder
   */
  exports.prototype['co_holder'] = undefined;
  /**
   * @member {module:model/Account.AccountTypeEnum} account_type
   */
  exports.prototype['account_type'] = undefined;
  /**
   * @member {String} creation_date
   */
  exports.prototype['creation_date'] = undefined;
  /**
   * @member {module:model/Account.StatusEnum} status
   */
  exports.prototype['status'] = undefined;
  /**
   * @member {String} description
   */
  exports.prototype['description'] = undefined;


  /**
   * Allowed values for the <code>account_type</code> property.
   * @enum {String}
   * @readonly
   */
  exports.AccountTypeEnum = {
    /**
     * value: "Checking account"
     * @const
     */
    "Checking account": "Checking account",
    /**
     * value: "Savings account"
     * @const
     */
    "Savings account": "Savings account",
    /**
     * value: "Individual Retirement Account (IRAs)"
     * @const
     */
    "Individual Retirement Account (IRAs)": "Individual Retirement Account (IRAs)"  };

  /**
   * Allowed values for the <code>status</code> property.
   * @enum {String}
   * @readonly
   */
  exports.StatusEnum = {
    /**
     * value: "ACTIVE"
     * @const
     */
    "ACTIVE": "ACTIVE",
    /**
     * value: "BLOCKED"
     * @const
     */
    "BLOCKED": "BLOCKED",
    /**
     * value: "NEGATIVE"
     * @const
     */
    "NEGATIVE": "NEGATIVE"  };


  return exports;
}));


