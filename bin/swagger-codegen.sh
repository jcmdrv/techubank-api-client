#!/bin/sh

mkdir ./tmp

wget http://central.maven.org/maven2/io/swagger/swagger-codegen-cli/2.3.1/swagger-codegen-cli-2.3.1.jar -O ./tmp/swagger-codegen-cli.jar


java -jar ./tmp/swagger-codegen-cli.jar generate \
   -i ./swagger.v2.yaml \
   -l javascript \
   -o . 

git add .
git commit -m "Creating new version"
git push origin develop

npm version patch

git add .
git commit -m "Creating new version"
git push origin develop

java -jar ./tmp/swagger-codegen-cli.jar generate \
  -i ./swagger.v2.yaml \
  -l nodejs-server \
  -o ./tmp/techubank-server-stub