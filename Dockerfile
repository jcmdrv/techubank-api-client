FROM node:8.12.0-alpine 


WORKDIR /techubank-api-client
ADD . /techubank-api-client


RUN npm install

CMD ["npm", "run", "test"]


