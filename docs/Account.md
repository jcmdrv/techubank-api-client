# TechubankApiClient.Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productType** | **String** |  | [optional] 
**accountNumber** | **String** |  | [optional] 
**title** | **String** |  | [optional] 
**holder** | [**UserBasicInfo**](UserBasicInfo.md) |  | [optional] 
**coHolder** | [**UserBasicInfo**](UserBasicInfo.md) |  | [optional] 
**accountType** | **String** |  | [optional] 
**creationDate** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**description** | **String** |  | [optional] 


<a name="AccountTypeEnum"></a>
## Enum: AccountTypeEnum


* `Checking account` (value: `"Checking account"`)

* `Savings account` (value: `"Savings account"`)

* `Individual Retirement Account (IRAs)` (value: `"Individual Retirement Account (IRAs)"`)




<a name="StatusEnum"></a>
## Enum: StatusEnum


* `ACTIVE` (value: `"ACTIVE"`)

* `BLOCKED` (value: `"BLOCKED"`)

* `NEGATIVE` (value: `"NEGATIVE"`)




