# TechubankApiClient.TransferDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**status** | **String** |  | [optional] 
**comments** | **String** |  | [optional] 


<a name="StatusEnum"></a>
## Enum: StatusEnum


* `DONE` (value: `"DONE"`)

* `REJECTED` (value: `"REJECTED"`)

* `PENDING` (value: `"PENDING"`)

* `CANCELED` (value: `"CANCELED"`)




