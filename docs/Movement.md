# TechubankApiClient.Movement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**origin** | **String** |  | [optional] 
**destination** | **String** |  | [optional] 


