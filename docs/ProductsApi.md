# TechubankApiClient.ProductsApi

All URIs are relative to *http://localhost:3000/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUserProducts**](ProductsApi.md#getUserProducts) | **GET** /products/{username} | Get user products


<a name="getUserProducts"></a>
# **getUserProducts**
> ArrayOfUserProducts getUserProducts(username)

Get user products

Get user products

### Example
```javascript
var TechubankApiClient = require('techubank_api_client');
var defaultClient = TechubankApiClient.ApiClient.instance;

// Configure API key authorization: APIKeyHeader
var APIKeyHeader = defaultClient.authentications['APIKeyHeader'];
APIKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//APIKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new TechubankApiClient.ProductsApi();

var username = "username_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getUserProducts(username, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 

### Return type

[**ArrayOfUserProducts**](ArrayOfUserProducts.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

