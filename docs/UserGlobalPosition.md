# TechubankApiClient.UserGlobalPosition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userInfo** | [**UserBasicInfo**](UserBasicInfo.md) |  | [optional] 
**products** | [**[UserProduct]**](UserProduct.md) |  | [optional] 


