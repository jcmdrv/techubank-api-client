# TechubankApiClient.ErrorMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**internalCode** | **String** |  | [optional] 
**message** | **String** |  | [optional] 


