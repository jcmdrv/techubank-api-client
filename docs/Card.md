# TechubankApiClient.Card

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cardNumber** | **String** |  | [optional] 
**title** | **String** |  | [optional] 
**holder** | [**UserBasicInfo**](UserBasicInfo.md) |  | [optional] 
**coHolder** | [**UserBasicInfo**](UserBasicInfo.md) |  | [optional] 
**cardType** | **String** |  | [optional] 
**creationDate** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**description** | **String** |  | [optional] 


<a name="CardTypeEnum"></a>
## Enum: CardTypeEnum


* `Debit` (value: `"Debit"`)

* `Prepaid` (value: `"Prepaid"`)




<a name="StatusEnum"></a>
## Enum: StatusEnum


* `ACTIVE` (value: `"ACTIVE"`)

* `BLOCKED` (value: `"BLOCKED"`)

* `NEGATIVE` (value: `"NEGATIVE"`)




