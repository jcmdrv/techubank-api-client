# TechubankApiClient.ProductsAccountTransferApi

All URIs are relative to *http://localhost:3000/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllTransfers**](ProductsAccountTransferApi.md#getAllTransfers) | **GET** /products/{username}/account/{accountId}/transfer | Get last transfers
[**getTransferDetail**](ProductsAccountTransferApi.md#getTransferDetail) | **GET** /products/{username}/account/{accountId}/transfer/{transferId} | Get detail info for a specific transfer
[**makeTransfer**](ProductsAccountTransferApi.md#makeTransfer) | **POST** /products/{username}/account/{accountId}/transfer | Create a transfer between two accounts
[**rejectTransfer**](ProductsAccountTransferApi.md#rejectTransfer) | **DELETE** /products/{username}/account/{accountId}/transfer/{transferId} | Reject a transfer


<a name="getAllTransfers"></a>
# **getAllTransfers**
> ArrayOfTransfers getAllTransfers(username, accountId)

Get last transfers

Get last transfers

### Example
```javascript
var TechubankApiClient = require('techubank_api_client');
var defaultClient = TechubankApiClient.ApiClient.instance;

// Configure API key authorization: APIKeyHeader
var APIKeyHeader = defaultClient.authentications['APIKeyHeader'];
APIKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//APIKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new TechubankApiClient.ProductsAccountTransferApi();

var username = "username_example"; // String | 

var accountId = "accountId_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAllTransfers(username, accountId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 
 **accountId** | **String**|  | 

### Return type

[**ArrayOfTransfers**](ArrayOfTransfers.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getTransferDetail"></a>
# **getTransferDetail**
> ArrayOfTransfers getTransferDetail(username, accountId, transferId)

Get detail info for a specific transfer

Get detail info for a specific transfer

### Example
```javascript
var TechubankApiClient = require('techubank_api_client');
var defaultClient = TechubankApiClient.ApiClient.instance;

// Configure API key authorization: APIKeyHeader
var APIKeyHeader = defaultClient.authentications['APIKeyHeader'];
APIKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//APIKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new TechubankApiClient.ProductsAccountTransferApi();

var username = "username_example"; // String | 

var accountId = "accountId_example"; // String | 

var transferId = "transferId_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getTransferDetail(username, accountId, transferId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 
 **accountId** | **String**|  | 
 **transferId** | **String**|  | 

### Return type

[**ArrayOfTransfers**](ArrayOfTransfers.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="makeTransfer"></a>
# **makeTransfer**
> TransferDetail makeTransfer(username, accountId, transferRequest)

Create a transfer between two accounts

Create a transfer between two accounts

### Example
```javascript
var TechubankApiClient = require('techubank_api_client');
var defaultClient = TechubankApiClient.ApiClient.instance;

// Configure API key authorization: APIKeyHeader
var APIKeyHeader = defaultClient.authentications['APIKeyHeader'];
APIKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//APIKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new TechubankApiClient.ProductsAccountTransferApi();

var username = "username_example"; // String | 

var accountId = "accountId_example"; // String | 

var transferRequest = new TechubankApiClient.TransferRequest(); // TransferRequest | Transfer info


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.makeTransfer(username, accountId, transferRequest, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 
 **accountId** | **String**|  | 
 **transferRequest** | [**TransferRequest**](TransferRequest.md)| Transfer info | 

### Return type

[**TransferDetail**](TransferDetail.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="rejectTransfer"></a>
# **rejectTransfer**
> rejectTransfer(username, accountId, transferId)

Reject a transfer

Reject a transfer

### Example
```javascript
var TechubankApiClient = require('techubank_api_client');
var defaultClient = TechubankApiClient.ApiClient.instance;

// Configure API key authorization: APIKeyHeader
var APIKeyHeader = defaultClient.authentications['APIKeyHeader'];
APIKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//APIKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new TechubankApiClient.ProductsAccountTransferApi();

var username = "username_example"; // String | 

var accountId = "accountId_example"; // String | 

var transferId = "transferId_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.rejectTransfer(username, accountId, transferId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 
 **accountId** | **String**|  | 
 **transferId** | **String**|  | 

### Return type

null (empty response body)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

