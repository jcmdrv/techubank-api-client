# TechubankApiClient.UserResetPassword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | [optional] 
**oldpassword** | **String** |  | [optional] 
**newpassword** | **String** |  | [optional] 
**renewpassword** | **String** |  | [optional] 


