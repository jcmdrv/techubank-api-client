# TechubankApiClient.ProductsAccountMovementsApi

All URIs are relative to *http://localhost:3000/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAccountMovementDetail**](ProductsAccountMovementsApi.md#getAccountMovementDetail) | **GET** /products/{username}/account/{accountId}/movements/{movementId} | Get detail user&#39;s movement info
[**getAccountMovements**](ProductsAccountMovementsApi.md#getAccountMovements) | **GET** /products/{username}/account/{accountId}/movements | Get user&#39;s account movements
[**rejectAccountMovement**](ProductsAccountMovementsApi.md#rejectAccountMovement) | **DELETE** /products/{username}/account/{accountId}/movements/{movementId} | Reject a movement


<a name="getAccountMovementDetail"></a>
# **getAccountMovementDetail**
> ArrayOfTransfers getAccountMovementDetail(username, accountId, movementId)

Get detail user&#39;s movement info

Get detail user movement info

### Example
```javascript
var TechubankApiClient = require('techubank_api_client');
var defaultClient = TechubankApiClient.ApiClient.instance;

// Configure API key authorization: APIKeyHeader
var APIKeyHeader = defaultClient.authentications['APIKeyHeader'];
APIKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//APIKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new TechubankApiClient.ProductsAccountMovementsApi();

var username = "username_example"; // String | 

var accountId = "accountId_example"; // String | 

var movementId = "movementId_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAccountMovementDetail(username, accountId, movementId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 
 **accountId** | **String**|  | 
 **movementId** | **String**|  | 

### Return type

[**ArrayOfTransfers**](ArrayOfTransfers.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAccountMovements"></a>
# **getAccountMovements**
> ArrayOfMovements getAccountMovements(username, accountId)

Get user&#39;s account movements

Get user movements. If there is no filter it get last month movements

### Example
```javascript
var TechubankApiClient = require('techubank_api_client');
var defaultClient = TechubankApiClient.ApiClient.instance;

// Configure API key authorization: APIKeyHeader
var APIKeyHeader = defaultClient.authentications['APIKeyHeader'];
APIKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//APIKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new TechubankApiClient.ProductsAccountMovementsApi();

var username = "username_example"; // String | 

var accountId = "accountId_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAccountMovements(username, accountId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 
 **accountId** | **String**|  | 

### Return type

[**ArrayOfMovements**](ArrayOfMovements.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="rejectAccountMovement"></a>
# **rejectAccountMovement**
> &#39;String&#39; rejectAccountMovement(username, accountId, movementId)

Reject a movement

Reject a movement

### Example
```javascript
var TechubankApiClient = require('techubank_api_client');
var defaultClient = TechubankApiClient.ApiClient.instance;

// Configure API key authorization: APIKeyHeader
var APIKeyHeader = defaultClient.authentications['APIKeyHeader'];
APIKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//APIKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new TechubankApiClient.ProductsAccountMovementsApi();

var username = "username_example"; // String | 

var accountId = "accountId_example"; // String | 

var movementId = "movementId_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.rejectAccountMovement(username, accountId, movementId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 
 **accountId** | **String**|  | 
 **movementId** | **String**|  | 

### Return type

**&#39;String&#39;**

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

