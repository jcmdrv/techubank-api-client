# TechubankApiClient.TransferRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | **String** |  | [optional] 
**to** | **String** |  | [optional] 
**requestor** | [**UserBasicInfo**](UserBasicInfo.md) |  | [optional] 
**destination** | **String** |  | [optional] 
**transferDate** | **String** |  | [optional] 
**description** | **String** |  | [optional] 


