# TechubankApiClient.UserProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productType** | **String** |  | 
**productValue** | **Object** |  | [optional] 


<a name="ProductTypeEnum"></a>
## Enum: ProductTypeEnum


* `Products - Account` (value: `"Products - Account"`)

* `CARD` (value: `"CARD"`)




