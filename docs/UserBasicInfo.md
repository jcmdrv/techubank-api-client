# TechubankApiClient.UserBasicInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**userStatus** | **String** |  | [optional] 


<a name="UserStatusEnum"></a>
## Enum: UserStatusEnum


* `ACTIVE` (value: `"ACTIVE"`)

* `BLOCKED` (value: `"BLOCKED"`)




