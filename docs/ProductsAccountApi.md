# TechubankApiClient.ProductsAccountApi

All URIs are relative to *http://localhost:3000/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAccountProduct**](ProductsAccountApi.md#createAccountProduct) | **POST** /products/{username}/account | Create user&#39;s product of type account
[**deleteAccount**](ProductsAccountApi.md#deleteAccount) | **DELETE** /products/{username}/account/{accountId} | Delete user&#39;s account
[**getAccountDetail**](ProductsAccountApi.md#getAccountDetail) | **GET** /products/{username}/account/{accountId} | Get user&#39;s account info
[**getAllUserAccounts**](ProductsAccountApi.md#getAllUserAccounts) | **GET** /products/{username}/account | Get all user&#39;s products of type account
[**updateAccountInfo**](ProductsAccountApi.md#updateAccountInfo) | **PUT** /products/{username}/account/{accountId} | Update user&#39;s account info


<a name="createAccountProduct"></a>
# **createAccountProduct**
> Account createAccountProduct(username, accountInfo)

Create user&#39;s product of type account

Create user&#39;s product of type account

### Example
```javascript
var TechubankApiClient = require('techubank_api_client');
var defaultClient = TechubankApiClient.ApiClient.instance;

// Configure API key authorization: APIKeyHeader
var APIKeyHeader = defaultClient.authentications['APIKeyHeader'];
APIKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//APIKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new TechubankApiClient.ProductsAccountApi();

var username = "username_example"; // String | 

var accountInfo = new TechubankApiClient.Account(); // Account | Logout user from the system


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.createAccountProduct(username, accountInfo, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 
 **accountInfo** | [**Account**](Account.md)| Logout user from the system | 

### Return type

[**Account**](Account.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteAccount"></a>
# **deleteAccount**
> ArrayOfTransfers deleteAccount(username, accountId)

Delete user&#39;s account

Delete user&#39;s account

### Example
```javascript
var TechubankApiClient = require('techubank_api_client');
var defaultClient = TechubankApiClient.ApiClient.instance;

// Configure API key authorization: APIKeyHeader
var APIKeyHeader = defaultClient.authentications['APIKeyHeader'];
APIKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//APIKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new TechubankApiClient.ProductsAccountApi();

var username = "username_example"; // String | 

var accountId = "accountId_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.deleteAccount(username, accountId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 
 **accountId** | **String**|  | 

### Return type

[**ArrayOfTransfers**](ArrayOfTransfers.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAccountDetail"></a>
# **getAccountDetail**
> ArrayOfTransfers getAccountDetail(username, accountId)

Get user&#39;s account info

Get user&#39;s account detail information

### Example
```javascript
var TechubankApiClient = require('techubank_api_client');
var defaultClient = TechubankApiClient.ApiClient.instance;

// Configure API key authorization: APIKeyHeader
var APIKeyHeader = defaultClient.authentications['APIKeyHeader'];
APIKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//APIKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new TechubankApiClient.ProductsAccountApi();

var username = "username_example"; // String | 

var accountId = "accountId_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAccountDetail(username, accountId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 
 **accountId** | **String**|  | 

### Return type

[**ArrayOfTransfers**](ArrayOfTransfers.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAllUserAccounts"></a>
# **getAllUserAccounts**
> ArrayOfUserAccounts getAllUserAccounts(username)

Get all user&#39;s products of type account

Get all user&#39;s products of type account

### Example
```javascript
var TechubankApiClient = require('techubank_api_client');
var defaultClient = TechubankApiClient.ApiClient.instance;

// Configure API key authorization: APIKeyHeader
var APIKeyHeader = defaultClient.authentications['APIKeyHeader'];
APIKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//APIKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new TechubankApiClient.ProductsAccountApi();

var username = "username_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAllUserAccounts(username, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 

### Return type

[**ArrayOfUserAccounts**](ArrayOfUserAccounts.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateAccountInfo"></a>
# **updateAccountInfo**
> ArrayOfTransfers updateAccountInfo(username, accountId)

Update user&#39;s account info

Update user&#39;s account info

### Example
```javascript
var TechubankApiClient = require('techubank_api_client');
var defaultClient = TechubankApiClient.ApiClient.instance;

// Configure API key authorization: APIKeyHeader
var APIKeyHeader = defaultClient.authentications['APIKeyHeader'];
APIKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//APIKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new TechubankApiClient.ProductsAccountApi();

var username = "username_example"; // String | 

var accountId = "accountId_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateAccountInfo(username, accountId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 
 **accountId** | **String**|  | 

### Return type

[**ArrayOfTransfers**](ArrayOfTransfers.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

