# TechubankApiClient.User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**lastName** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**password** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**userStatus** | **String** |  | [optional] 


<a name="UserStatusEnum"></a>
## Enum: UserStatusEnum


* `ACTIVE` (value: `"ACTIVE"`)

* `BLOCKED` (value: `"BLOCKED"`)




