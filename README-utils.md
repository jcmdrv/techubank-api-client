# Important notes

Most of the files in this project are automatically created by wager-codegen.

We are in chart of integration tests which can be executed with 'npm run test'.

# Requirements
- Node.js 8.4+
- Java 8
- Docker

# Write the scaffolding again

The swagger.yaml must be on the base path.

```
techubank > ./bin/swagger-codegen.sh
```

Swagger-codegen will ignore the following paths:
- tmp
- test/**/*
- package.json

We are going to write our integration tests in the *tests* folder.

# Integration tests

These tests launch a techubank-api server docker image, executes a set of integration tests and shut down the container.

```
techubank > npm install
techubank > npm run test
```